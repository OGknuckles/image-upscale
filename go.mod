module bitbucket.org/OGknuckles/image-upscale

go 1.13

require (
	github.com/pkg/errors v0.9.1
	github.com/unchartedsoftware/plog v0.0.0-20200807135627-83d59e50ced5
)
